#! /bin/bash

# Generates several images to find a good threshold

for i in `seq 1 99`
do
	echo $i
	./exec input/GT_00$i.png output/$i.png
done
