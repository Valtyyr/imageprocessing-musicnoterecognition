#include "rectangle.hh"

Rectangle::Rectangle(int right, int bottom, int left, int top)
  : right_(right),
    bottom_(bottom),
    left_(left),
    top_(top)
{}

void Rectangle::findMiniMaxi(int row, int col) {
  if (right_ < col) {
    right_ = col;
  }

  if (bottom_ < row) {
    bottom_ = row;
  }

  if (left_ > col) {
    left_ = col;
  }

  if (top_ > row) {
    top_ = row;
  }
}
