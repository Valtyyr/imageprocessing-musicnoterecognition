#include <mln/core/image/image2d.hh>
#include <mln/core/alias/window2d.hh>
#include <mln/core/alias/neighb2d.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <mln/value/label_16.hh>
#include <mln/data/all.hh>
#include <mln/literal/colors.hh>
#include <mln/draw/box.hh>

#include <mln/io/magick/load.hh>
#include <mln/io/magick/save.hh>

#include <iostream>
#include <deque>
#include <mln/morpho/dilation.hh>
#include <mln/morpho/erosion.hh>
#include <mln/opt/at.hh>

#include <rectangle.hh>

using namespace mln;

void drawRect(image2d<value::rgb8>& img, Rectangle* rect, value::rgb8 color) {
  draw::line(img, point2d(rect->top_, rect->left_), point2d(rect->top_, rect->right_), color);
  draw::line(img, point2d(rect->top_ - 1, rect->left_ - 1), point2d(rect->top_ - 1, rect->right_ + 1), color);
  draw::line(img, point2d(rect->top_ + 1, rect->left_ + 1), point2d(rect->top_ + 1, rect->right_ - 1), color);

  draw::line(img, point2d(rect->top_, rect->left_), point2d(rect->bottom_, rect->left_), color);
  draw::line(img, point2d(rect->top_ - 1, rect->left_ - 1), point2d(rect->bottom_ + 1, rect->left_ - 1), color);
  draw::line(img, point2d(rect->top_ + 1, rect->left_ + 1), point2d(rect->bottom_ - 1, rect->left_ + 1), color);

  draw::line(img, point2d(rect->top_, rect->right_), point2d(rect->bottom_, rect->right_), color);
  draw::line(img, point2d(rect->top_ - 1, rect->right_ + 1), point2d(rect->bottom_ + 1, rect->right_ + 1), color);
  draw::line(img, point2d(rect->top_ + 1, rect->right_ - 1), point2d(rect->bottom_ - 1, rect->right_ - 1), color);

  draw::line(img, point2d(rect->bottom_, rect->left_), point2d(rect->bottom_, rect->right_), color);
  draw::line(img, point2d(rect->bottom_ + 1, rect->left_ - 1), point2d(rect->bottom_ + 1, rect->right_ + 1), color);
  draw::line(img, point2d(rect->bottom_ - 1, rect->left_ + 1), point2d(rect->bottom_ - 1, rect->right_ - 1), color);
}

void fillShapeWithoutDeque(image2d<value::int_u8>& img, int row, int col) {
  if (row >= 0 && col >= 0 && row < img.nrows() && col < img.ncols()
    && opt::at(img, row, col) != 0) {
    opt::at(img, row, col) = 0;

    fillShapeWithoutDeque(img, row - 1, col);
    fillShapeWithoutDeque(img, row + 1, col);
    fillShapeWithoutDeque(img, row, col - 1);
    fillShapeWithoutDeque(img, row, col + 1);
  }
}

void fillShape(image2d<value::int_u8>& img, int row, int col, std::deque<point2d>& shapePoint,  Rectangle* rect) {
  if (row >= 0 && col >= 0 && row < img.nrows() && col < img.ncols()
    && opt::at(img, row, col) != 0) {
    point2d p(row, col);

    opt::at(img, row, col) = 0;
    rect->findMiniMaxi(row, col);

    shapePoint.push_back(p);

    fillShape(img, row - 1, col, shapePoint, rect);
    fillShape(img, row + 1, col, shapePoint, rect);
    fillShape(img, row, col - 1, shapePoint, rect);
    fillShape(img, row, col + 1, shapePoint, rect);
  }
}

int countLine(image2d<value::int_u8>& inputDoubleLine, std::deque<point2d>& shapeLine, std::deque<point2d>& shapePos) {
  int count = 0;

  for (auto it = shapeLine.begin(); it != shapeLine.end(); ++it) {
    if (opt::at(inputDoubleLine, it->row(), it->col()) == 255) {
      fillShapeWithoutDeque(inputDoubleLine, it->row(), it->col());
      shapePos.push_back(*it);
      ++count;
    }
  }
  return count;
}

void fillSplit(image2d<value::int_u8>& img,
 int row,
 int col,
 Rectangle* rect) {
  if (row >= 0 && col >= 0 && row < img.nrows() && col < img.ncols()
    && opt::at(img, row, col) != 0) {
    point2d p(row, col);

    opt::at(img, row, col) = 0;
    rect->findMiniMaxi(row, col);

    fillSplit(img, row - 1, col, rect);
    fillSplit(img, row + 1, col, rect);
    fillSplit(img, row, col - 1, rect);
    fillSplit(img, row, col + 1, rect);
  }
}

bool isBarRect(Rectangle* rect) {
  return rect->bottom_ - rect->top_ >= 95
  && rect->right_ - rect->left_ < 25;
}

bool rectContainCircle(Rectangle* rect, image2d<value::int_u8>& inputCircle) {
  bool res = false;

  for (int j = rect->top_; j <= rect->bottom_; ++j) {
    for (int i = rect->left_; i <= rect->right_; ++i) {
      if (opt::at(inputCircle, j, i) == 255) {
        res = true;
      }
    }
  }

  return res;
}

void splitRect(image2d<value::int_u8>& inputCircle,
 image2d<value::int_u8>& inputShape,
 image2d<value::int_u8>& splitShape,
 image2d<value::rgb8>& inputResult,
 std::deque<point2d>& shapePos,
 int nbElement,
 Rectangle* rect) 
  if (nbElement >= 1) 
    std::deque<point2d> shapeLine;
    std::deque<Rectangle*> rectQueue;
    Rectangle* rectSplited;
    bool doubleLine = true;
    value::rgb8 colorSplit = value::rgb8(0, 255, 0);

    for (auto it = shapePos.begin(); it != shapePos.end(); ++it) 
      rectSplited = new Rectangle(0, 0, inputResult.ncols() - 1, inputResult.nrows() - 1);
      fillSplit(splitShape, it->row(), it->col(), rectSplited);

      if (rectSplited->bottom_ - rectSplited->top_ < 30)
        doubleLine = false;

      if (rectSplited->right_ - rectSplited->left_ >= 35)
        colorSplit = value::rgb8(0, 0, 255);
    }

    if (doubleLine) 
      for (int j = rect->top_; j <= rect->bottom_; ++j) 
        for (int i = rect->left_; i <= rect->right_; ++i) 
          if (opt::at(inputShape, j, i) == 255) 
            rectSplited = new Rectangle(0, 0, inputResult.ncols() - 1, inputResult.nrows() - 1);
            fillSplit(inputShape, j, i, rectSplited);

            if ((colorSplit == value::rgb8(0, 255, 0)
              && (rectSplited->bottom_ - rectSplited->top_ < 95
                || rectSplited->right_ - rectSplited->left_ >= 25
                || rectSplited->right_ - rectSplited->left_ < 6))
              || rectContainCircle(rectSplited, inputCircle))
              colorSplit = value::rgb8(0, 0, 255);

            rectQueue.push_back(rectSplited);
          }
        }
      }

      if (colorSplit == value::rgb8(0, 255, 0))
        colorSplit = value::rgb8(255, 0, 0);

      for (auto it = rectQueue.begin(); it != rectQueue.end(); ++it) 
        drawRect(inputResult, *it, colorSplit);
      }
    }
    else
      drawRect(inputResult, rect, value::rgb8(0, 0, 255));

    delete rectSplited;
  }
  else 
    drawRect(inputResult, rect, value::rgb8(0, 0, 255));
  }
}

bool containCircle(std::deque<point2d> shapeLine, image2d<value::int_u8>& inputCircle) 
  bool res = false;

  for (auto it = shapeLine.begin(); it != shapeLine.end(); ++it) {
    if (opt::at(inputCircle, it->row(), it->col()) == 255) {
      res = true;
    }
  }

  return res;
}

void findLineInShape(image2d<value::int_u8>& inputCircle,
 image2d<value::int_u8>& inputDoubleLine,
 image2d<value::int_u8>& inputLine,
 image2d<value::int_u8>& inputShape,
 image2d<value::int_u8>& splitShape,
 image2d<value::rgb8>& inputResult,
 std::deque<point2d>& shapePoint,
 Rectangle* rectShape) {
  std::deque<point2d> shapeLine;
  std::deque<point2d> shapePos;
  Rectangle* rect = 0;
  point2d p;
  int nbElement;

  while (!shapePoint.empty()) {
    rect = new Rectangle(0, 0, inputResult.ncols() - 1, inputResult.nrows() - 1);
    p = shapePoint.front();

    if (opt::at(inputLine, p.row(), p.col()) == 255) {
      fillShape(inputLine, p.row(), p.col(), shapeLine, rect);

      nbElement = countLine(inputDoubleLine, shapeLine, shapePos);

      if (isBarRect(rectShape)
        && !containCircle(shapeLine, inputCircle)
        && nbElement == 1)
        drawRect(inputResult, rect, value::rgb8(255, 0, 0));
      else if (rect->bottom_ - rect->top_ >= 800
        && rect->right_ - rect->left_ < 50) {
        drawRect(inputResult, rect, value::rgb8(255, 0, 0));
      }
      else
        splitRect(inputCircle, inputShape, splitShape, inputResult, shapePos, nbElement, rect);

      shapePos.erase(shapePos.begin(), shapePos.end());
      shapeLine.erase(shapeLine.begin(), shapeLine.end());
    }

    shapePoint.pop_front();
  }

  delete rect;
}

image2d<value::int_u8> detectLine(std::string file, int threshold) {
  image2d<value::int_u8> inputLine;
  io::magick::load(inputLine, file);

  win::vline2d vlineErosion(threshold);
  inputLine = morpho::erosion(inputLine, vlineErosion);

  // Threshold
  win::vline2d vlineDilation(threshold + 11);
  inputLine = morpho::dilation(inputLine, vlineDilation);

  // Threshold
  win::hline2d hlineDilation(15);
  inputLine = morpho::dilation(inputLine, hlineDilation);

  return inputLine;
}

image2d<value::int_u8> detectSplit(std::string file, int threshold) {
  image2d<value::int_u8> inputLine;
  io::magick::load(inputLine, file);

  win::vline2d vlineErosion(threshold);
  inputLine = morpho::erosion(inputLine, vlineErosion);

  // Threshold
  win::vline2d vlineDilation(threshold + 11);
  inputLine = morpho::dilation(inputLine, vlineDilation);

  return inputLine;
}

image2d<value::int_u8> detectDoubleLine(std::string file) {
  image2d<value::int_u8> inputDoubleLine;
  io::magick::load(inputDoubleLine, file);

  // Threshold
  win::vline2d vlineDilation(5);
  inputDoubleLine = morpho::dilation(inputDoubleLine, vlineDilation);

  return inputDoubleLine;
}

image2d<value::int_u8> detectCircle(std::string file) {
  image2d<value::int_u8> inputCircle;
  io::magick::load(inputCircle, file);

  win::hline2d hlineErosion(11);
  win::hline2d hlineDilation(17);
  inputCircle = morpho::dilation(morpho::erosion(inputCircle, hlineErosion), hlineDilation);

  win::vline2d vlineErosion(11);
  win::vline2d vlineDilation(17);
  inputCircle = morpho::dilation(morpho::erosion(inputCircle, vlineErosion), vlineDilation);

  return inputCircle;
}

int main(int argc, char* argv[]) {
  if (argc != 3)
  {
    std::cout << "usage: " << argv[0] << " filein fileout" <<std::endl;
    return 1;
  }

  image2d<value::int_u8> input;
  image2d<value::int_u8> inputDoubleLine;
  image2d<value::int_u8> inputLine;
  image2d<value::int_u8> inputCircle;
  image2d<value::int_u8> inputShape;
  image2d<value::int_u8> splitShape;
  image2d<value::rgb8> inputResult;
  std::deque<point2d> shapePoint;
  Rectangle* rect = 0;

  io::magick::load(input, argv[1]);
  io::magick::load(inputResult, argv[1]);

  win::vline2d vline(3);
  input = morpho::dilation(input, vline);

  // Threshold
  inputDoubleLine = detectDoubleLine(argv[1]);
  splitShape = detectDoubleLine(argv[1]);
  inputLine = detectLine(argv[1], 19);
  inputShape = detectSplit(argv[1], 19);
  inputCircle = detectCircle(argv[1]);

  mln_piter_(box2d) p(input.domain());

  for_all (p) {
    value::int_u8 val = opt::at(inputLine, p.row(), p.col());

    if (val != 0 && opt::at(input, p.row(), p.col()) == 255) {
      rect = new Rectangle(0, 0, inputResult.ncols() - 1, inputResult.nrows() - 1);
      fillShape(input, p.row(), p.col(), shapePoint, rect);

      findLineInShape(inputCircle, inputDoubleLine, inputLine, inputShape, splitShape, inputResult, shapePoint, rect);
    }
  }

  delete rect;

  io::magick::save(inputResult, argv[2]);

  return 0;
}
