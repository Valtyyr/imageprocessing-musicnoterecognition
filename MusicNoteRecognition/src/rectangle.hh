#ifndef RECTANGLE_HH_
# define RECTANGLE_HH_

# include <iostream>

class Rectangle
{
  public:
    Rectangle(int right, int bottom, int left, int top);
    void findMiniMaxi(int row, int col);
  //private:
    int right_;
    int bottom_;
    int left_;
    int top_;
};

#endif // !RECTANGLE_HH_
